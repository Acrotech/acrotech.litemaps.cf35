﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Acrotech.LiteMaps
{
    public static class ExtensionMethods
    {
        public static bool WaitOne(this WaitHandle source, int millisecondsTimeout)
        {
            return source.WaitOne(millisecondsTimeout, false);
        }
    }
}

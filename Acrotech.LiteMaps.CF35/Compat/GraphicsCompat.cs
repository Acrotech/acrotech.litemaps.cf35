﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Acrotech.LiteMaps.Compat
{
    public static class GraphicsCompat
    {
        public static void FillEllipse(this Graphics g, Brush brush, float x, float y, float width, float height)
        {
            g.FillEllipse(brush, (int)x, (int)y, (int)width, (int)height);
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.WinForms.Layers;
using Resco.UIElements;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.PortableViewModel.Commands;
using Acrotech.LiteMaps.WinForms;
using DotSpatial.Positioning;

namespace Acrotech.LiteMaps.CF35.TestApp
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            Disposed += OnDispose;

            Initialize();
        }

        private MapViewModel ViewModel { get; set; }
        private GpsSimulator Gps { get; set; }

        private void Initialize()
        {
            Map.ViewModel = ViewModel = new MapViewModel(
                new GoogleTileImageSource(
                    GoogleTileType.Satellite, 
                    GoogleTileImageSource.DefaultSatelliteVersion, 
                    GoogleTileImageSource.DefaultLanguage
                )
            );

            ViewModel.PropertyChanged += OnMapPropertyChanged;

            Map.AddLayer(new FPSLayer(Map.ViewPort, LayerCornerPosition.TopLeft));
            Map.AddLayer(new TileDownloadStatusLayer(Map.ViewPort, LayerCornerPosition.BottomLeft));
            Map.AddLayer(new TargetMarkerLayer());
            Map.AddLayer(new GpsMarkerLayer());

            Gps = new GpsSimulator();
            Gps.PositionChanged += OnGpsPositionChanged;

            ViewModel.TargetPosition = Gps.CurrentPosition;
            //ViewModel.TargetAccuracy = Gps.CurrentAccuracy;
            
            Gps.Start();

            BindCommand(ViewModel.ZoomIn, ZoomInButton);
            BindCommand(ViewModel.ZoomOut, ZoomOutButton);
            BindCommand(ViewModel.CenterOnGps, CenterOnGPSButton);
            BindCommand(ViewModel.CenterOnTarget, CenterOnTargetButton);
        }

        private void OnMapPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "GpsPosition")
            {
                RunOnUIThread(() => DisplayText.Text = ViewModel.GpsPosition.ToString("d.dddddd"));
            }
        }

        private void OnGpsPositionChanged(Position position, Distance accuracy)
        {
            ViewModel.GpsPosition = position;
            ViewModel.GpsAccuracy = accuracy;
        }

        private void BindCommand(ICommand command, UIButton button)
        {
            command.CanExecuteChanged += (s, e) => RunOnUIThread(() => button.Enabled = command.CanExecute(null));
            button.Click += (s, e) => command.Execute(null);

            button.Enabled = command.CanExecute(null);
        }

        private void RunOnUIThread(Action action)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(action);
                }
                catch (ObjectDisposedException)
                {
                }
            }
            else
            {
                action();
            }
        }

        private void OnDispose(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.PropertyChanged -= OnMapPropertyChanged;
            }

            if (Gps != null)
            {
                Gps.PositionChanged -= OnGpsPositionChanged;

                Gps.Dispose();
                Gps = null;
            }
        }
    }
}
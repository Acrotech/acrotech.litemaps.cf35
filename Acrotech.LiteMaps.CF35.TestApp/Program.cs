﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.PortableLogAdapter.Managers;
using Acrotech.PortableLogAdapter;
using System.Diagnostics;

namespace Acrotech.LiteMaps.CF35.TestApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Initialize();

            Application.Run(new MainWindow());
        }

        public static ILogManager LogManager { get; private set; }

        private static void Initialize()
        {
            LogManager = new DelegateLogManager((_, x) => Debug.WriteLine(x));

            Services.Initialize(LogManager);
        }
    }
}
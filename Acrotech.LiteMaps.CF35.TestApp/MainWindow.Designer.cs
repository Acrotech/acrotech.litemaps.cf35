﻿namespace Acrotech.LiteMaps.CF35.TestApp
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Map = new Acrotech.LiteMaps.WinForms.MapView();
            this.uiElementPanelControl1 = new Resco.UIElements.Controls.UIElementPanelControl();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.ZoomInButton = new Resco.UIElements.UIButton();
            this.ZoomOutButton = new Resco.UIElements.UIButton();
            this.CenterOnGPSButton = new Resco.UIElements.UIButton();
            this.CenterOnTargetButton = new Resco.UIElements.UIButton();
            this.DisplayText = new Resco.UIElements.UILabel();
            this.uiElementPanelControl1.SuspendElementLayout();
            this.SuspendLayout();
            // 
            // Map
            // 
            this.Map.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Map.Location = new System.Drawing.Point(0, 0);
            this.Map.Name = "Map";
            this.Map.Size = new System.Drawing.Size(480, 538);
            this.Map.TabIndex = 0;
            // 
            // uiElementPanelControl1
            // 
            this.uiElementPanelControl1.Children.Add(this.uiGridPanel1);
            this.uiElementPanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiElementPanelControl1.Location = new System.Drawing.Point(0, 538);
            this.uiElementPanelControl1.Name = "uiElementPanelControl1";
            this.uiElementPanelControl1.Size = new System.Drawing.Size(480, 50);
            this.uiElementPanelControl1.TabIndex = 1;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.ZoomInButton, 0, 0);
            this.uiGridPanel1.Children.Add(this.ZoomOutButton, 1, 0);
            this.uiGridPanel1.Children.Add(this.CenterOnGPSButton, 3, 0);
            this.uiGridPanel1.Children.Add(this.CenterOnTargetButton, 4, 0);
            this.uiGridPanel1.Children.Add(this.DisplayText, 2, 0);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 480, 50);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.TouchScrollSensitivity = 20;
            // 
            // ZoomInButton
            // 
            this.ZoomInButton.BorderThickness = 2;
            this.ZoomInButton.DisabledBackground.BorderThickness = 2;
            this.ZoomInButton.FocusedBackground.BorderThickness = 4;
            this.ZoomInButton.IsFocusable = false;
            this.ZoomInButton.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 50, 50);
            this.ZoomInButton.Name = "ZoomInButton";
            this.ZoomInButton.Text = "+";
            // 
            // ZoomOutButton
            // 
            this.ZoomOutButton.BorderThickness = 2;
            this.ZoomOutButton.DisabledBackground.BorderThickness = 2;
            this.ZoomOutButton.FocusedBackground.BorderThickness = 4;
            this.ZoomOutButton.IsFocusable = false;
            this.ZoomOutButton.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 50, 50);
            this.ZoomOutButton.Name = "ZoomOutButton";
            this.ZoomOutButton.TabIndex = 1;
            this.ZoomOutButton.Text = "-";
            // 
            // CenterOnGPSButton
            // 
            this.CenterOnGPSButton.BorderThickness = 2;
            this.CenterOnGPSButton.DisabledBackground.BorderThickness = 2;
            this.CenterOnGPSButton.FocusedBackground.BorderThickness = 4;
            this.CenterOnGPSButton.IsFocusable = false;
            this.CenterOnGPSButton.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 50, 50);
            this.CenterOnGPSButton.Name = "CenterOnGPSButton";
            this.CenterOnGPSButton.TabIndex = 2;
            this.CenterOnGPSButton.Text = "GPS";
            // 
            // CenterOnTargetButton
            // 
            this.CenterOnTargetButton.BorderThickness = 2;
            this.CenterOnTargetButton.DisabledBackground.BorderThickness = 2;
            this.CenterOnTargetButton.FocusedBackground.BorderThickness = 4;
            this.CenterOnTargetButton.IsFocusable = false;
            this.CenterOnTargetButton.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 50, 50);
            this.CenterOnTargetButton.Name = "CenterOnTargetButton";
            this.CenterOnTargetButton.TabIndex = 3;
            this.CenterOnTargetButton.Text = "Tgt";
            // 
            // DisplayText
            // 
            this.DisplayText.AutoSize = false;
            this.DisplayText.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.DisplayText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 280, 50);
            this.DisplayText.Name = "DisplayText";
            this.DisplayText.TabIndex = 4;
            this.DisplayText.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.Map);
            this.Controls.Add(this.uiElementPanelControl1);
            this.Location = new System.Drawing.Point(0, 52);
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Acrotech.LiteMaps";
            this.uiElementPanelControl1.ResumeElementLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Acrotech.LiteMaps.WinForms.MapView Map;
        private Resco.UIElements.Controls.UIElementPanelControl uiElementPanelControl1;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UIButton ZoomInButton;
        private Resco.UIElements.UIButton ZoomOutButton;
        private Resco.UIElements.UIButton CenterOnGPSButton;
        private Resco.UIElements.UIButton CenterOnTargetButton;
        private Resco.UIElements.UILabel DisplayText;
    }
}

